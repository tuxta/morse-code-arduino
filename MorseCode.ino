/*
 * This Program was made to easily scale to any number of LEDs
 * and any set of Morse Code strings.
 * It was deliberately done using pointer arithetic just to
 * make it as efficient as possible.
 * AND ... No Magic Numbers!! \o/
 * 
 *  Author: Steven Tucker
 */


// Library required for string functions
#include <string.h>

// List terminater for iteration using pointer arithetic
const int LIST_END = -1;

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
                                                 //////
////////////////////////////////////////////     //////
// The sentence to convert to Morse Code  //     //////
////////////////////////////////////////////     //////
const int num_chars = 3;                         //////
char sentence[num_chars + 1] = "sos";            //////
                                                 //////
////////////////////////////                     //////
//  Set LED PIN Numbers.  //                     //////
// End list with LIST_END //                     //////
////////////////////////////                     //////
int leds[] = { 6, LIST_END };                    //////
const int buzzer = 3;                            //////
const int button = 8;
                                                 //////
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

// Time light is on for a dash
int dash = 400;
// Time light is on for dot
int dot = 150;
// Wait time in between codes
int code_change = 500;
// Time to wait for a space
int space_delay = 100;
// Delay period before repeating the code string
int wait_time = 2000;



// Will point to the array of dots and dashes
// That represent the sentence
int *code;
int code_len = 0;

//Function declarations
void add_code(int *symbol_code, int count, int code_index);
int * convert_to_code(int *code, char *sentence);
void set_all_leds(int power);

/*
 * Set each LED Pin to OUTPUT
 */
void setup() {
  //Convert the list of characters, so the code timings (dashes, dots, light off time)
  convert_to_code(sentence);
  
  // Set each LED PIN to OUTPUT
  int * list_item = leds;
  while(*list_item != LIST_END){
    pinMode(*list_item, OUTPUT);
    ++list_item;
  }
  pinMode(button, INPUT);
}

/*
 * Light LEDs according to the code list
 * Wait before repeating
 */
void loop() {

  // Wait for button to be pressed
  if(digitalRead(button) == HIGH) {
    // Send each item of the code
    int *curr_code = code;
    while(*curr_code != LIST_END) {
      if(*curr_code == 0) {
        delay(space_delay);
      } else {
        set_all_leds(HIGH);
        tone(buzzer, 1000);
        delay(*curr_code);
        set_all_leds(LOW);
        noTone(buzzer);
        delay(code_change);
      }
      curr_code++;
    }
  }
}

/*
 * Switch all LEDs On or Off
 * @param power - HIGH or LOW for On or OFF
 */
void set_all_leds(int power){
  int *list_item = leds;
  while(*list_item != LIST_END) {
    digitalWrite(*list_item, power);
    ++list_item;
  }
}

//////////////////
// Set up codes //
//////////////////
int a[2] = { dot, dash }; int a_len = 2;
int b[4] = { dash, dot, dot, dot }; int b_len = 4;
int c[4] = { dash, dot, dash, dot }; int c_len = 4;
int d[3] = { dash, dot, dot }; int d_len = 3;
int e[1] = { dot }; int e_len = 1;
int f[4] = { dot, dot, dash, dot }; int f_len = 4;
int g[3] = { dash, dash, dot }; int g_len = 3;
int h[4] = { dot, dot, dot, dot }; int h_len = 4;
int i[2] = { dot, dot }; int i_len = 2;
int j[4] = { dot, dash, dash, dash }; int j_len = 4;
int k[3] = { dash, dot, dash }; int k_len = 3;
int l[4] = { dot, dash, dot, dot }; int l_len = 4;
int m[2] = { dash, dash }; int m_len = 2;
int n[2] = { dash, dot }; int n_len = 2;
int o[3] = { dash, dash, dash }; int o_len = 3;
int p[4] = { dot, dash, dash, dot }; int p_len = 4;
int q[4] = { dash, dash, dot, dash }; int q_len = 4;
int r[3] = { dot, dash, dot }; int r_len = 3;
int s[3] = { dot, dot, dot }; int s_len = 3;
int t[1] = { dash }; int t_len = 1;
int u[3] = { dot, dot, dash }; int u_len = 3;
int v[4] = { dot, dot, dot, dash }; int v_len = 4;
int w[3] = { dot, dash, dash }; int w_len = 3;
int x[4] = { dash, dot, dot, dash }; int x_len = 4;
int y[4] = { dash, dot, dash, dash }; int y_len = 4;
int z[4] = { dash, dash, dot, dot }; int z_len = 4;

int No_0[5] = { dash, dash, dash, dash, dash }; int No_0_len = 5;
int No_1[5] = { dot, dash, dash, dash, dash }; int No_1_len = 5;
int No_2[5] = { dot, dot, dash, dash, dash }; int No_2_len = 5;
int No_3[5] = { dot, dot, dot, dash, dash }; int No_3_len = 5;
int No_4[5] = { dot, dot, dot, dot, dash }; int No_4_len = 5;
int No_5[5] = { dot, dot, dot, dot, dot }; int No_5_len = 5;
int No_6[5] = { dash, dot, dot, dot, dot }; int No_6_len = 5;
int No_7[5] = { dash, dash, dot, dot, dot }; int No_7_len = 5;
int No_8[5] = { dash, dash, dash, dot, dot }; int No_8_len = 5;
int No_9[5] = { dash, dash, dash, dash, dot }; int No_9_len = 5;

int * convert_to_code(char *sentence) {
  // Allocate memory to hold the code
  code_len = 40;
  code = (int*)calloc(code_len, sizeof(int));
  int *code_index = code;
  int curr_len = 0;

  int curr_char = 0;
  while(curr_char < num_chars) {
    
    // Increase the array size if needed - preferably in place
    if(curr_len * sizeof(int) >= code_len - 10 * sizeof(int)) {
      code_len += sizeof(int) * 20;
      code = (int*)realloc(code, code_len);
    }

    switch(sentence[curr_char]) {
      case 'a' :
      case 'A' :
          add_code(a, a_len, &code_index, &curr_len);
          code_index += a_len;
          break;
      case 'b' :
      case 'B' :
          add_code(b, b_len, &code_index, &curr_len);
          code_index += b_len;
          break;
      case 'c' :
      case 'C' :
          add_code(c, c_len, &code_index, &curr_len);
          code_index += c_len;
          break;
      case 'd' :
      case 'D' :
          add_code(d, d_len, &code_index, &curr_len);
          code_index += d_len;
          break;
      case 'e' :
      case 'E' :
          add_code(e, e_len, &code_index, &curr_len);
          code_index += e_len;
          break;
      case 'f' :
      case 'F' :
          add_code(f, f_len, &code_index, &curr_len);
          code_index += f_len;
          break;
      case 'g' :
      case 'G' :
          add_code(g, g_len, &code_index, &curr_len);
          code_index += g_len;
          break;
      case 'h' :
      case 'H' :
          add_code(h, h_len, &code_index, &curr_len);
          code_index += h_len;
          break;
      case 'i' :
      case 'I' :
          add_code(i, i_len, &code_index, &curr_len);
          code_index += i_len;
          break;
      case 'j' :
      case 'J' :
          add_code(j, j_len, &code_index, &curr_len);
          code_index += j_len;
          break;
      case 'k' :
      case 'K' :
          add_code(k, k_len, &code_index, &curr_len);
          code_index += k_len;
          break;
      case 'l' :
      case 'L' :
          add_code(l, l_len, &code_index, &curr_len);
          code_index += l_len;
          break;
      case 'm' :
      case 'M' :
          add_code(m, m_len, &code_index, &curr_len);
          code_index += m_len;
          break;
      case 'n' :
      case 'N' :
          add_code(n, n_len, &code_index, &curr_len);
          code_index += n_len;
          break;
      case 'o' :
      case 'O' :
          add_code(o, o_len, &code_index, &curr_len);
          code_index += o_len;
          break;
      case 'p' :
      case 'P' :
          add_code(p, p_len, &code_index, &curr_len);
          code_index += p_len;
          break;
      case 'q' :
      case 'Q' :
          add_code(q, q_len, &code_index, &curr_len);
          code_index += q_len;
          break;
      case 'r' :
      case 'R' :
          add_code(r, r_len, &code_index, &curr_len);
          code_index += r_len;
          break;
      case 's' :
      case 'S' :
          add_code(s, s_len, &code_index, &curr_len);
          code_index += s_len;
          break;
      case 't' :
      case 'T' :
          add_code(t, t_len, &code_index, &curr_len);
          code_index += t_len;
          break;
      case 'u' :
      case 'U' :
          add_code(u, u_len, &code_index, &curr_len);
          code_index += u_len;
          break;
      case 'v' :
      case 'V' :
          add_code(v, v_len, &code_index, &curr_len);
          code_index += v_len;
          break;
      case 'w' :
      case 'W' :
          add_code(w, w_len, &code_index, &curr_len);
          code_index += w_len;
          break;
      case 'x' :
      case 'X' :
          add_code(x, x_len, &code_index, &curr_len);
          code_index += x_len;
          break;
      case 'y' :
      case 'Y' :
          add_code(y, y_len, &code_index, &curr_len);
          code_index += y_len;
          break;
      case 'z' :
      case 'Z' :
          add_code(z, z_len, &code_index, &curr_len);
          code_index += z_len;
          break;
      case ' ' :
          *code_index = 0;
          code_index += sizeof(int);
          ++curr_len;
          break;
      case '0' :
          add_code(No_0, No_0_len, &code_index, &curr_len);
          code_index += No_0_len;
          break;
      case '1' :
          add_code(No_1, No_1_len, &code_index, &curr_len);
          code_index += No_1_len;
          break;
      case '2' :
          add_code(No_2, No_2_len, &code_index, &curr_len);
          code_index += No_2_len;
          break;
      case '3' :
          add_code(No_3, No_3_len, &code_index, &curr_len);
          code_index += No_3_len;
          break;
      case '4' :
          add_code(No_4, No_4_len, &code_index, &curr_len);
          code_index += No_4_len;
          break;
      case '5' :
          add_code(No_5, No_5_len, &code_index, &curr_len);
          code_index += No_5_len;
          break;
      case '6' :
          add_code(No_6, No_6_len, &code_index, &curr_len);
          code_index += No_6_len;
          break;
      case '7' :
          add_code(No_7, No_7_len, &code_index, &curr_len);
          code_index += No_7_len;
          break;
      case '8' :
          add_code(No_8, No_8_len, &code_index, &curr_len);
          code_index += No_8_len;
          break;
      case '9' :
          add_code(No_9, No_9_len, &code_index, &curr_len);
          code_index += No_9_len;
          break;
      
      default :
          break;
    }
    curr_char++;
  }
  
  *code_index = LIST_END;
  return code;
}

// Copy the letter code to the morse code array
void add_code(int *symbol_code, int count, int **code_index, int *curr_len) {
  memcpy(*code_index, symbol_code, sizeof(int) * count);
  *code_index += count * sizeof(int);
  *curr_len += count;
}
